#!/usr/bin/php
<?php
/*  teebridge.php - An IRC bot interface between an IRC channel and TeeWorlds.
    Copyright (C) 2013 Michael Marley <michael@michaelmarley.com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

$ircservers=array("morgan.freenode.net","rajaniemi.freenode.net","leguin.freenode.net","holmes.freenode.net","hobana.freenode.net","gibson.freenode.net","cameron.freenode.net","adams.freenode.net","roddenberry.freenode.net");
$ircserverindex=0;
$ircport=6667;
$nickservpassword="youwish";
$botnick="TeeBridge";
$botname="TeeWorlds Bridge Bot";
$configfile="teebridge.conf";

$weapons=array("hammer","pistol","shotgun","grenade launcher","laser","katana");
$weaponkillmessages=array(array("flattened",""),array("shot",""),array("pumped"," full of lead"),array("blew"," to bits"),array("burned a hole through",""),array("slashed"," to shreds"));
$items=array(array("heart"),array("shield"),$weapons,$weapons);
$messagetypes=array("connection","namechange","team","chatdown","chatup","ipickup","wpickup","frag","flag","balance","gameover");

class Config{
	public $admins;
	public $channels;
	
	public function addadmin($admin){
		if(!array_key_exists($admin,$this->admins)){
			$this->admins[$admin]=array();
			$this->storeconfig();
			ircauto("\"$admin\" is now an admin.");
			return true;
		}else{
			ircauto("\"$admin\" is already an admin.");
			return false;
		}
	}
	
	public function removeadmin($admin){
		if(array_key_exists($admin,$this->admins)){
			if(count($this->admins)==1){
				ircauto("\"$admin\" is the only remaining admin; cannot remove.");
				return false;
			}
			unset($this->admins[$admin]);
			$this->storeconfig();
			ircauto("\"$admin\" is no longer an admin.");
			return false;
		}else{
			ircauto("\"$admin\" is not an admin.");
			return false;
		}
	}
	
	public function listadmins(){
		$response="Admins are: ";
		foreach($this->admins as $key=>$value){
			$response.="$key ";
		}
		ircauto($response);
	}
	
	public function checkadmin($nick){
		if(array_key_exists($nick,$this->admins)){
			return true;
		}else{
			ircauto("$nick is not in the sudoers file.  This incident will be reported.",false);
			return false;
		}
	}
	
	public function setadmindata($admin,$data){
		if(array_key_exists($admin,$this->admins)){
			$this->admins[$admin]=$data;
			$this->storeconfig();
		}
	}
	
	public function addchannel($channel){
		if(!substr($channel,0,1)=="#"){
			ircauto("Invalid channel name \"$channel.\"");
			return false;
		}
		irccommand("JOIN $channel");
		if(!array_key_exists($channel,$this->channels)){
			$this->channels[$channel]=array();
			$this->storeconfig();
			ircauto("Joined channel \"$channel.\"");
			return true;
		}else{
			ircauto("Already in channel \"$channel.\"");
			return false;
		}
	}
	
	public function removechannel($channel){
		irccommand("PART $channel");
		if(array_key_exists($channel,$this->channels)){
			unset($this->channels[$channel]);
			$this->storeconfig();
			ircauto("Parted channel \"$channel.\"");
			return true;
		}else{
			ircauto("Not in channel \"$channel.\"");
			return false;
		}
	}
	
	public function listchannels(){
		$response="Currently in channels: ";
		foreach($this->channels as $key=>$value){
			$response.="$key ";
		}
		ircauto($response);
	}
	
	public function setchanneldata($channel,$data){
		if(array_key_exists($channel,$this->channels)){
			$this->channels[$channel]=$data;
			$this->storeconfig();
		}
	}
	
	public function loadconfig(){
		if(file_exists($GLOBALS['configfile'])){
			$loadedconfig=unserialize(file_get_contents($GLOBALS['configfile']));
			$this->admins=$loadedconfig->admins;
			$this->channels=$loadedconfig->channels;
		}else{
			$this->admins=array();
			$this->admins["mamarley"]=array();
			$this->channels=array();
			$this->channels["#botters-test"]=array();
			$this->storeconfig();
		}
	}
	
	public function joinchannels(){
		irccommand("JOIN " . implode(",",array_keys($this->channels)));
	}
	
	public function storeconfig(){
		file_put_contents($GLOBALS['configfile'],serialize($this));
	}
}

$pongsmissed=0;
$nullarray=array();
$lastnewroundtime=time();

$twinput=fopen("/tmp/teeworldstoteebridge.pipe","r+");
$twoutput=fopen("/tmp/teebridgetoteeworlds.pipe","r+");

$config=new Config();
$config->loadconfig();

ircconnect();

while(true){
	$connectionarray=array($ircconnection,$twinput);
	if(stream_select($connectionarray,$nullarray,$nullarray,30)){
		if(in_array($twinput,$connectionarray)){
			print($twinputline=fgets($twinput));
			$messagearray=explode(": ",trim($twinputline),2);
			$messagetype=str_replace("]","",explode("][",$messagearray[0],2)[1]);
			if($messagetype=="chat"||$messagetype=="teamchat"){
				if(str_startswith($messagearray[1],"***")){
					$username=esubstr($messagearray[1],strpos($messagearray[1],'\'')+1,strrpos($messagearray[1],'\''));
					if(strstr($username,"' changed name to '")){
						$namearray=explode("' changed name to '",$username,2);
						ircbroadcast("namechange",$namearray[0] . " is now known as " . $namearray[1]);
					}else if(strstr($messagearray[1],"entered and joined the")){
						if(time()-$lastnewroundtime>2){
							ircbroadcast("connection","[$username connected]");
							if(str_endswith($messagearray[1],"red team")){
								ircbroadcast("team","$username joined the red team.");
							}else if(str_endswith($messagearray[1],"blue team")){
								ircbroadcast("team","$username joined the blue team.");
							}
						}
					}else if(str_endswith($messagearray[1],"joined the red team")){
						ircbroadcast("team","$username switched to the red team.");
					}else if(str_endswith($messagearray[1],"joined the blue team")){
						ircbroadcast("team","$username switched to the blue team.");
					}else if(str_endswith($messagearray[1],"joined the spectators")){
						ircbroadcast("team","$username is now spectating.");
					}else if(str_endswith($messagearray[1],"joined the game")){
						ircbroadcast("team","$username is no longer spectating.");
					}else if(str_endswith($messagearray[1],"has left the game")){
						ircbroadcast("connection","[$username disconnected]");
					}
				}else{
					$chat=explode(":",$messagearray[1],3)[2];
					$chatarray=explode(": ",$chat,2);
					ircbroadcast("chatdown","(" . $chatarray[0] . ") " . $chatarray[1]);
				}
			}else if($messagetype=="game"){
				if(str_startswith($messagearray[1],"start round type='")){
					$lastnewroundtime=time();
				}
				if(str_startswith($messagearray[1],"pickup player='")&&strstr($messagearray[1],"item=")){
					$username=explode(":",esubstr($messagearray[1],strpos($messagearray[1],'\'')+1,strrpos($messagearray[1],'\'')),2)[1];
					$pickuparray=sscanf(trim(esubstr($messagearray[1],strrpos($messagearray[1],'\'')+1,strlen($messagearray[1]))),"item=%d/%d");
					ircbroadcast(($pickuparray[0]>1 ? "wpickup":"ipickup"),"$username picked up a " . $items[$pickuparray[0]][$pickuparray[1]]);
				}else if(str_startswith($messagearray[1],"kill killer='")&&strstr($messagearray[1],"victim='")&&strstr($messagearray[1],"weapon=")){
					$killarray=explode("'",$messagearray[1]);
					$weaponarray=sscanf(trim($killarray[4]),"weapon=%d special=%d");
					if($weaponarray[0]>=0){
						ircbroadcast("frag",str_replace("\t","'",explode(":",$killarray[1],2)[1]) . " " . $weaponkillmessages[$weaponarray[0]][0] . " " . str_replace("\t","'",explode(":",$killarray[3],2)[1]) . $weaponkillmessages[$weaponarray[0]][1] . " with the " . $weapons[$weaponarray[0]]);
					}else if($weaponarray[0]==-1){
						ircbroadcast("frag",str_replace("\t","'",explode(":",$killarray[1],2)[1]) . " stepped in the wrong place");
					}
				}else if(str_startswith($messagearray[1],"flag_grab player='")){
					$username=explode(":",esubstr($messagearray[1],strpos($messagearray[1],'\'')+1,strrpos($messagearray[1],'\'')),2)[1];
					ircbroadcast("flag","$username grabbed the opposing team's flag!");
				}else if(str_startswith($messagearray[1],"flag_capture player='")){
					$username=explode(":",esubstr($messagearray[1],strpos($messagearray[1],'\'')+1,strrpos($messagearray[1],'\'')),2)[1];
					ircbroadcast("flag","$username captured the opposing team's flag!");
				}else if(str_startswith($messagearray[1],"Teams are NOT balanced (red=")){
					$balancearray=sscanf($messagearray[1],"Teams are NOT balanced (red=%d blue=%d)");
					ircbroadcast("balance","Teams unbalanced!  Red team has " . $balancearray[0] . " and blue team has " . $balancearray[1]);
				}else if(str_startswith($messagearray[1],"Balancing teams")){
					ircbroadcast("balance","Team balance forced!");
				}else if(str_startswith($messagearray[1],"Game over, red team wins")){
					ircbroadcast("gameover","Game over!  Red team wins!");
				}else if(str_startswith($messagearray[1],"Game over, blue team wins")){
					ircbroadcast("gameover","Game over!  Blue team wins!");
				}else if(str_startswith($messagearray[1],"Game over, winner is")){
					ircbroadcast("gameover","Game over!  " . str_replace("Game over, winner is ","",$messagearray[1]) . " wins!");
				}
			}else if($messagetype=="datafile"){
				if(str_startswith($messagearray[1],"loading data index=")){
					$lastnewroundtime=time();
				}
			}
		}else if(in_array($ircconnection,$connectionarray)){
			print($line=fgets($ircconnection));
			$linearray=explode(" :",trim($line),2);
			$prefixarray=explode(" ",$linearray[0]);
			if($linearray[0]=="PING"){
				irccommand("PONG :$linearray[1]");
				continue;
			}
			if(count($prefixarray)>=2){
				$command=$prefixarray[1];
				if($command=="PONG"){
					$pongsmissed=0;
					continue;
				}
				if($command=="433"){
					$curbotnick=$curbotnick . "_";
					irccommand("NICK " . $curbotnick);
					$config->joinchannels();
					irccommand("PRIVMSG NickServ :GHOST " . $botnick . " " . $nickservpassword);
					continue;
				}
				if($command=="437"){
					$curbotnick=$linearray[0];
					nickservauth();
					$curbotnick=$botnick;
					irccommand("NICK " . $curbotnick);
					continue;
				}
			}
			if(count($linearray)==2){
				if(($command=="NOTICE")&&(strstr($linearray[1],"has been ghosted"))){
					$curbotnick=$botnick;
					irccommand("NICK " . $curbotnick);
					continue;
				}
				if(($command=="NICK")&&(strstr($linearray[0],"!~" . $botnick . "@"))){
					$curbotnick=$linearray[1];
					continue;
				}
				if(($linearray[0]=="ERROR")&&(stristr($linearray[1],"Closing Link"))){
					ircreconnect();
					continue;
				}
			}
			$senderarray=explode("!",$prefixarray[0]);
			if(count($senderarray)==1){
				continue;
			}
			$sender=trim(substr($senderarray[0],1));
			if($sender==$curbotnick){
				continue;
			}
			if(count($prefixarray)>=3){
				$target=$prefixarray[2];
			}else{
				continue;
			}
			if($target==$curbotnick){
				$target=$sender;
			}else{
				if(substr($target,0,1)=="+"||substr($target,0,1)=="@"){
					$target=substr($target,1);
				}
			}
			if(($command=="PRIVMSG")||($command=="NOTICE")){
				$responsecommand=$command;
				if(count($linearray)==2){
					$message=stripircformatting($linearray[1]);
					if(str_startswith(strtolower($message),strtolower($curbotnick) . ":")){
						$botcommandstring=str_ireplace("$curbotnick:","",$message);
					}else if(str_startswith(strtolower($message),strtolower($curbotnick) . ",")){
						$botcommandstring=str_ireplace("$curbotnick,","",$message);
					}else if($target==$sender){
						$botcommandstring=$message;
					}else{
						$botcommandstring=null;
					}
					if(isset($botcommandstring)){
						$botcommandstring=removedupespaces($botcommandstring);
						$botcommandarray=explode(" ",trim($botcommandstring));
						$botcommandarraylength=count($botcommandarray);
						$botcommand=strtolower($botcommandarray[0]);
						if($botcommand=="help"){
							if($botcommandarraylength==1){
								ircauto("Valid commands are \"help\", \"source\", \"listadmins\", \"addmin <nick>\", \"removemin <nick>\", \"listchannels\", \"join <#channel>\", \"part <#channel>\", \"listmtypes <#channel>\", \"enablemtype <#channel> <mtype>\", \"disablemtype <#channel> <mtype>.\"");
							}else{
								ircauto("Invalid command!  Syntax is \"help.\"");
							}
						}else if($botcommand=="source"){
							if($botcommandarraylength==1){
								ircauto("https://github.com/mamarley/TeeBridge");
							}else{
								ircauto("Invalid command!  Syntax is \"source.\"");
							}
						}else if($botcommand=="listadmins"){
							if($botcommandarraylength==1){
								$config->listadmins();
							}else{
								ircauto("Invalid command!  Syntax is \"listadmins.\"");
							}
						}else if($botcommand=="addmin"){
							if($botcommandarraylength==2){
								if($config->checkadmin($sender)){
									$config->addadmin($botcommandarray[1]);
								}
							}else{
								ircauto("Invalid command!  Syntax is \"addmin <nick>.\"");
							}
						}else if($botcommand=="removemin"){
							if($botcommandarraylength==2){
								if($config->checkadmin($sender)){
									$config->removeadmin($botcommandarray[1]);
								}
							}else{
								ircauto("Invalid command!  Syntax is \"removemin <nick>.\"");
							}
						}else if($botcommand=="listchannels"){
							if($botcommandarraylength==1){
								$config->listchannels();
							}else{
								ircauto("Invalid command!  Syntax is \"listchannels.\"");
							}
						}else if($botcommand=="join"){
							if($botcommandarraylength==2){
								if($config->checkadmin($sender)){
									$config->addchannel($botcommandarray[1]);
								}
							}else{
								ircauto("Invalid command!  Syntax is \"join <#channel>.\"");
							}
						}else if($botcommand=="part"){
							if($botcommandarraylength==2){
								if($config->checkadmin($sender)){
									$config->removechannel($botcommandarray[1]);
								}
							}else{
								ircauto("Invalid command!  Syntax is \"part <#channel>.\"");
							}
						}else if($botcommand=="listmtypes"){
							if($botcommandarraylength==2){
								if(isset($config->channels[$botcommandarray[1]])){
									$response="Enabled message types for channel \"$botcommandarray[1]:\" ";
									foreach($config->channels[$botcommandarray[1]] as $value){
										$response.=$value . " ";
									}
									ircauto($response);
								}else{
									ircauto("Not in channel \"$botcommandarray[1]!\"");
								}
							}else{
								ircauto("Invalid command!  Syntax is \"listmtypes <#channel>.\"");
							}
						}else if($botcommand=="enablemtype"){
							if($botcommandarraylength==3){
								if($config->checkadmin($sender)){
									if(isset($config->channels[$botcommandarray[1]])){
										if(!in_array($botcommandarray[2],$config->channels[$botcommandarray[1]])){
											if(in_array($botcommandarray[2],$messagetypes)){
												$currentmessagetypes=$config->channels[$botcommandarray[1]];
												array_push($currentmessagetypes,$botcommandarray[2]);
												$config->setchanneldata($botcommandarray[1],$currentmessagetypes);
												ircauto("Message type \"$botcommandarray[2]\" is now enabled on channel \"$botcommandarray[1].\"");
											}else if($botcommandarray[2]=="all"){
												$config->setchanneldata($botcommandarray[1],$messagetypes);
												ircauto("All message types are now enabled on channel \"$botcommandarray[1].\"");
											}else{
												ircauto("\"$botcommandarray[2]\" is an invalid message type!");
											}
										}else{
											ircauto("Message type \"$botcommandarray[2]\" is already enabled on channel \"$botcommandarray[1]!\"");
										}
									}else{
										ircauto("Not in channel \"$botcommandarray[1]!\"");
									}
								}
							}else{
								ircauto("Invalid command!  Syntax is \"enablemtype <#channel> <mtype>.\"");
							}
						}else if($botcommand=="disablemtype"){
							if($botcommandarraylength==3){
								if($config->checkadmin($sender)){
									if(isset($config->channels[$botcommandarray[1]])){
										if((in_array($botcommandarray[2],$config->channels[$botcommandarray[1]]))||($botcommandarray[2]=="all")){
											if(in_array($botcommandarray[2],$messagetypes)){
												$currentmessagetypes=$config->channels[$botcommandarray[1]];
												foreach($currentmessagetypes as $key=>$value){
													if($value==$botcommandarray[2]){
														unset($currentmessagetypes[$key]);
													}
												}
												$config->setchanneldata($botcommandarray[1],$currentmessagetypes);
												ircauto("Message type \"$botcommandarray[2]\" is now disabled on channel \"$botcommandarray[1].\"");
											}else if($botcommandarray[2]=="all"){
												$config->setchanneldata($botcommandarray[1],array());
												ircauto("All message types now disabled on channel \"$botcommandarray[1].\"");
											}else{
												ircauto("\"$botcommandarray[2]\" is an invalid message type!");
											}
										}else{
											ircauto("Message type \"$botcommandarray[2]\" is not enabled on channel \"$botcommandarray[1]!\"");
										}
									}else{
										ircauto("Not in channel \"$botcommandarray[1]!\"");
									}
								}
							}else{
								ircauto("Invalid command!  Syntax is \"disablemytpe <#channel> <mtype>.\"");
							}
						}else if(str_startswith($botcommand,"\001PING")){
							if($botcommandarraylength==2){
								ircnotice("\001PING " . str_replace("\001","",$botcommandarray[1]) . "\001");
							}else{
								ircnotice("\001ERRMSG Invalid CTCP command!  Syntax is \"PING <timestamp>\"\001");
							}
						}else if(str_startswith($botcommand,"\001VERSION")){
							if($botcommandarraylength==1){
								ircnotice("\001VERSION worthlessbot PHP IRC bot:git:PHP " . phpversion() . " " . php_uname("srm") . " " . php_uname("r") . " " . php_uname("m") . "\001");
							}else{
								ircnotice("\001ERRMSG Invalid CTCP command!  Syntax is \"VERSION\"\001");
							}
						}else if(str_startswith($botcommand,"\001TIME")){
							if($botcommandarraylength==1){
								ircnotice("\001TIME " . date("D M d H:i:s Y T") . "\001");
							}else{
								ircnotice("\001ERRMSG Invalid CTCP command!  Syntax is \"TIME\"\001");
							}
						}else if(str_startswith($botcommand,"\001CLIENTINFO")){
							ircnotice("\001CLIENTINFO Use VERSION instead.\001");
						}else if(str_startswith($botcommand,"\001USERINFO")){
							if($botcommandarraylength==1){
								ircnotice("\001USERINFO $botname\001");
							}else{
								ircnotice("\001ERRMSG Invalid CTCP command!  Syntax is \"USERINFO\"\001");
							}
						}else if(str_startswith($botcommand,"\001SOURCE")){
							if($botcommandarraylength==1){
								ircnotice("\001ERRMSG Not available over FTP, use \"$botname: source\" for a URL.\001");
							}else{
								ircnotice("\001ERRMSG Invalid CTCP command!  Syntax is \"SOURCE\"\001");
							}
						}else{
							ircauto("Invalid command!  Try \"help.\"");
						}
					}else{
						if(in_array("chatup",$config->channels[$target])){
							if(str_startswith($message,"\001ACTION")){
								fwrite($twoutput,"($target) $sender" . str_replace(array("\001ACTION","\001"),"",$message) . "\0");
							}else{
								fwrite($twoutput,"($target) <$sender> $message\0");
							}
						}
					}
				}
			}else if((($command=="KICK")&&($prefixarray[3]==$curbotnick))||(($command=="PART")&&($sender==$curbotnick)&&(str_startswith($linearray[1],"requested by")))){
				irccommand("JOIN $prefixarray[2]");
			}
		}else{
			print("WARNING: stream_select() indicated data available but none is, reconnecting!\n");
			ircreconnect();
		}
	}else{
		if($pongsmissed<2){
			irccommand("PING $curbotnick");
			$pongsmissed++;
		}else{
			print("WARNING: Too many pongs missed, reconnecting!\n");
			ircreconnect();
		}
		if($curbotnick!=$botnick){
			$curbotnick=$botnick;
			irccommand("NICK " . $curbotnick);
		}
	}
}

function str_startswith($str,$sub){
    return (substr($str,0,strlen($sub))==$sub);
}

function str_endswith($str,$sub){
    return (substr($str,strlen($str)-strlen($sub))==$sub);
}

function esubstr($str,$beg,$end){
	return substr($str,$beg,$end-$beg);
}

function ircconnect(){
	while(true){
		$GLOBALS['ircconnection']=fsockopen($GLOBALS['ircservers'][$GLOBALS['ircserverindex']],$GLOBALS['ircport']);
		if($GLOBALS['ircconnection']==false){
			$GLOBALS['ircserverindex']++;
			if($GLOBALS['ircserverindex']>=count($GLOBALS['ircservers'])){
				$GLOBALS['ircserverindex']=0;
			}
		}else{
			break;
		}
	}
	$GLOBALS['pongsmissed']=0;
	$GLOBALS['curbotnick']=$GLOBALS['botnick'];
	
	irccommand("NICK " . $GLOBALS['curbotnick']);
	irccommand("PASS " . $GLOBALS['curbotnick'] . ":" . $GLOBALS['nickservpassword']);
	irccommand("USER " . $GLOBALS['curbotnick'] . " 8 * : " . $GLOBALS['botname']);
	
	$GLOBALS['config']->joinchannels();
}

function ircdisconnect(){
	fclose($GLOBALS['ircconnection']);
}

function ircreconnect(){
	ircdisconnect();
	ircconnect();
}

function irccommand($command){
	print($command . "\n");
	fwrite($GLOBALS['ircconnection'],"$command\n");
}

function ircprivmsg($message,$target=null){
	ircmessage("PRIVMSG",$target,$message);
}

function ircnotice($message,$target=null){
	ircmessage("NOTICE",$target,$message);
}

function ircmessage($type,$target,$message){
	if($message!=""){
		if($target==null){
			$target=$GLOBALS['target'];
		}
		irccommand("$type $target :$message");
	}
}

function ircauto($message,$address=true){
	if($address){
		$message=$GLOBALS['sender'] . ": $message";
	}
	if($GLOBALS['responsecommand']=="NOTICE"){
		ircnotice($message);
	}else{
		ircprivmsg($message);
	}
}

function nickservauth(){
	irccommand("PRIVMSG NickServ :IDENTIFY $botnick $nickservpassword");
}

function ircbroadcast($messagetype,$message){
	foreach($GLOBALS['config']->channels as $key=>$value){
		if(in_array($messagetype,$value)){
			irccommand("PRIVMSG $key :$message");
		}
	}
}

function stripircformatting($text){
    return preg_replace(array('/(\x03(?:\d{1,2}(?:,\d{1,2})?)?)/','/\x02/','/\x0F/','/\x16/','/\x1F/'),'',$text);
}

function removedupespaces($text){
	return preg_replace('!\s+!',' ',$text);
}
?>
